import gitlab
url="http://gitlab.com"
token="94j53ZKFG7iFhZU-faE6"
#log in
gl=gitlab.Gitlab(url, token)
#------------------------------------------------- --------------- #
#Get first page project
projects=gl.projects.list()
#Get all projects
projects=gl.projects.list(all=True)
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Get the name and id of all projects
for p in gl.projects.list(all=True, as_list=False):
  print (p.name, p.id)
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Get the name and id of the first page project
for p in gl.projects.list(page=1):
  print(p.name, p.id)
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Get the project object by specifying the id
project=gl.projects.get(501)
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Find items
projects=gl.projects.list(search="keyword")
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Create a project
project=gl.projects.create({"name":"project1"})
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Get public items
projects=gl.projects.list(visibility="public") #public, internal or private
#------------------------------------------------- --------------- #
#Get the project object is the basis of the following operations
#------------------------------------------------- --------------- #
#Get all branches of the project by specifying the project object
branches=project.branches.list()
print(branches)
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Get the attributes of the specified branch
branch=project.branches.get("master")
print(branch)
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Create branch
branch=project.branches.create({"branch_name":"feature1", "ref":"master"})
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Delete branch
project.branches.delete ("feature1")
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Branch protection/cancel protection
branch.protect ()
branch.unprotect ()
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Get all tags for the specified item
tags=project.tags.list ()
#Get information for a specific tag
tags=project.tags.list ("1.0")
#Create a tag
tag=project.tags.create ({"tag_name":"1.0", "ref":"master"})
#Set tags Description:
tag.set_release_description ("awesome v1.0 release")
#Delete tags
project.tags.delete ("1.0")
#or
tag.delete ()
#------------------------------------------------- --------------- #
#Get all commit info
commits=project.commits.list ()
for c in commits:
  print (c.author_name, c.message, c.title)
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Get the info of the specified commit
commit=project.commits.get ("e3d5a71b")
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Get all merge requests for the specified project
mrs=project.mergerequests.list ()
print (mrs)
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Get specified mr info
mr=project.mergerequests.get (mr_id)
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Create a merge request
mr=project.mergerequests.create ({"source_branch":"cool_feature",                  "target_branch":"master",                  "title":"merge cool feature",})
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Update the description of a merge request
mr.description="new description"
mr.save ()
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Switch a merge request (close or reopen):
mr.state_event="close" #or "reopen"
mr.save ()
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#delete a mr:
project.mergerequests.delete (mr_id)
#or
mr.delete ()
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#accept a mr:
mr.merge ()
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Filter all specified merge requests
#state:state of the mr. it can be one of all, merged, opened or closed
#order_by:sort by created_at or updated_at
#sort:sort order (asc or desc)
mrs=project.mergerequests.list (state="merged", sort="asc") #all, merged, opened or closed
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#Create a commit
data={
  "branch_name":"master", #v3
  "commit_message":"blah blah blah",  "actions":[
    {
      "action":"create",      "file_path":"blah",      "content":"blah"
    }
  ]
}
commit=project.commits.create (data)
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#compare two branches, tags or commits:
result=project.repository_compare ("develop", "feature-20180104")
print (result)
#get the commits
for commit in result ["commits"]:
  print (commit)
#
#get the diffs
for file_diff in result ["diffs"]:
  print (file_diff)
#------------------------------------------------- --------------- #
#------------------------------------------------- --------------- #
#get the commits
for commit in result ["commits"]:
  print (commit)
#
#get the diffs
for file_diff in result ["diffs"]:
  print (file_diff)
#------------------------------------------------- --------------- #
